import React from 'react';
import { shallow, configure } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import { Button, Popover } from '@material-ui/core';

configure({ adapter: new Adapter() });
const MenuButton = require('../index').default;

describe('menu button component', () => {
  it('renders default options correctly', () => {
    const mockFn = jest.fn();
    const component = shallow(
      <MenuButton classes={{}} menuItems={[]} selectedFilter='' handleMenuSelect={mockFn} />
    );
    const tree = shallowToJson(component);
    expect(tree).toMatchSnapshot();
  });
  it('menu button click toggles menu status', () => {
    const event = {
      currentTarget: {}
    };
    const menuMock = [{ id: 1, name: 'Test' }];
    const mockFn = jest.fn();
    const component = shallow(
      <MenuButton classes={{}} menuItems={menuMock} selectedFilter='' handleMenuSelect={mockFn} />
    ).dive();
    component.find(Button).props().onClick(event);
    expect(component.state().isMenuOpen).toEqual(event.currentTarget);
    component.find(Popover).props().onClose();
    expect(component.state().isMenuOpen).toEqual(null);
  });
});
