import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import {
  Icon,
  Typography,
  Button,
  MenuItem,
  ListItemText,
  Popover
} from '@material-ui/core'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'

import styles from './styles'

class MenuButton extends Component {
  state = {
    isMenuOpen: null
  }

  handleClick = (event) => {
    this.setState({ isMenuOpen: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ isMenuOpen: null })
  }

  onMenuButtonClick = (item) => {
    const { handleMenuSelect, name } = this.props
    this.handleClose()
    handleMenuSelect(item, name)
  }

  render() {
    const {
      classes,
      menuItems,
      buttonColor,
      margin,
      textColor,
      selectedFilter,
      listItemWidth,
      buttonStyle,
      height,
      width,
      border,
      fontSize,
      isDisabled,
      minWidth,
      showDisabledOptionStyles,
      borderRadius,
      alignOrigin
    } = this.props
    const { isMenuOpen } = this.state
    const isWarehouseButton = buttonStyle === 'warehouseButton'
    return (
      <div className={classes.flexContainer}>
        <Button
          disabled={isDisabled}
          className={
            isWarehouseButton ? classes.buttonForWarehouse : classes.button
          }
          variant='outlined'
          style={{
            backgroundColor: buttonColor,
            height,
            border,
            width,
            margin,
            minWidth,
            borderRadius: !isWarehouseButton && borderRadius
          }}
          onClick={this.handleClick}
        >
          {(menuItems.length > 0 || isDisabled) && (
            <Typography
              className={classes.buttontext}
              style={{ color: textColor, fontSize }}
            >
              <FormattedMessage
                id={`menuItems.${selectedFilter}`}
                defaultMessage={selectedFilter}
              />
            </Typography>
          )}
          {!isDisabled && (
            <Icon style={{ color: textColor, fontSize: 18 }}>
              arrow_drop_down
            </Icon>
          )}
        </Button>
        {isMenuOpen && (
          <Popover
            anchorEl={isMenuOpen}
            open={Boolean(isMenuOpen)}
            onClose={this.handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: alignOrigin
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: alignOrigin
            }}
          >
            {menuItems &&
              menuItems.map((item) => {
                const itemKey = item.name
                  ? item.name
                  : typeof item === 'string'
                  ? item
                  : null
                if (itemKey) {
                  return (
                    <MenuItem
                      key={item.id || item}
                      onClick={() => this.onMenuButtonClick(item)}
                      style={listItemWidth && { width: listItemWidth }}
                    >
                      <ListItemText
                        classes={{
                          primary:
                            showDisabledOptionStyles &&
                            !item.enabled &&
                            classes.disabled
                        }}
                        primary={
                          <FormattedMessage
                            id={`menuItems.${itemKey}`}
                            defaultMessage={itemKey}
                          />
                        }
                      />
                    </MenuItem>
                  )
                }
              })}
          </Popover>
        )}
      </div>
    )
  }
}

MenuButton.propTypes = {
  selectedFilter: PropTypes.string,
  handleMenuSelect: PropTypes.func.isRequired,
  classes: PropTypes.shape().isRequired,
  menuItems: PropTypes.arrayOf(PropTypes.shape()),
  buttonColor: PropTypes.string,
  textColor: PropTypes.string,
  buttonStyle: PropTypes.string,
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  minWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  border: PropTypes.string,
  borderRadius: PropTypes.number,
  fontSize: PropTypes.number,
  isDisabled: PropTypes.bool,
  margin: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  name: PropTypes.string,
  showDisabledOptionStyles: PropTypes.bool,
  listItemWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  alignOrigin: PropTypes.string
}

MenuButton.defaultProps = {
  selectedFilter: '',
  menuItems: [],
  buttonStyle: '',
  buttonColor: '#757575',
  textColor: '#FFFFFF',
  height: 'max-content',
  border: '',
  margin: '',
  width: 'max-content',
  minWidth: 'none',
  fontSize: 14,
  isDisabled: false,
  name: '',
  showDisabledOptionStyles: false,
  borderRadius: 0,
  listItemWidth: 'max-content',
  alignOrigin: 'center'
}

export default withStyles(styles)(MenuButton)
