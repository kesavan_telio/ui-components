import React from 'react';
import { storiesOf } from '@storybook/react';
import { IntlProvider } from 'react-intl';
import { HashRouter as Router } from 'react-router-dom';
import { messagesVi } from '../../translation/locales/vi';
import MenuButton from './menuButton';

import { menuItems } from '../../../../mockdata/menuItems';

const messages = {
  vi: messagesVi
};
const language = localStorage.getItem('language') || 'vi';

storiesOf('MenuButton', module)
  .add('Dark', () => (
    <IntlProvider locale={language} messages={messages[language]}>
      <Router>
        <MenuButton
          buttonColor='#404448'
          menuItems={menuItems}
        />
      </Router>
    </IntlProvider>
  ))
  .add('Default', () => (
    <IntlProvider locale={language} messages={messages[language]}>
      <Router>

        <MenuButton
          menuItems={menuItems}
        />
      </Router>
    </IntlProvider>
  )).add('Disabled', () => (
    <IntlProvider locale={language} messages={messages[language]}>
      <Router>

        <MenuButton
          menuItems={menuItems}
          isDisabled={true}
        />
      </Router>
    </IntlProvider>
  ));
