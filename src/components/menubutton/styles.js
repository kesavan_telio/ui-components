const styles = () => ({
  flexContainer: {
    display: 'flex'
  },
  button: {
    justifyContent: 'space-between',
    borderRadius: 0,
    '&:hover': {
      opacity: 0.8
    }
  },
  buttonForWarehouse: {
    justifyContent: 'space-between',
    padding: '0px 10px 0px 15px',
    margin: '0px 5px 0px 5px',
    borderRadius: 30
  },
  buttontext: {
    textTransform: 'none',
    display: 'flex',
    alignItems: 'center',
    fontSize: '14px',
    letterSpacing: 0.17,
    color: '#484C4F',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    maxWidth: '100%'
  },
  disabled: {
    color: '#A3AAB1'
  }
});

export default styles;
