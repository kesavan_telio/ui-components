const styles = () => ({
  searchIcon: {
    padding: 0,
    color: '#32AAD3',
    marginLeft: 10
  },
  textFieldRoot: {
    padding: 5,
    backgroundColor: '#FFFFFF',
    borderRadius: 5
  },
  inputField: {
    fontSize: 14,
    padding: 5,
    minWidth: '200px',
  },
  iconButton: {
    padding: 0
  }
});

export default styles;
