/* eslint-disable global-require */
import React from 'react';
import { shallow, configure } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
const intl = {
  formatMessage: () => { }
};
const mockFn = jest.fn();
describe('Searchbar', () => {
  it('renders default options correctly', () => {
    const Searchbar = require('../index').default;
    const component = shallow(
      <Searchbar classes={{}} intl={intl} onChangeSearch={() => { }} onClickSearch={() => { }} />
    );
    const tree = shallowToJson(component);
    expect(tree).toMatchSnapshot();
  });
  it('onChangeSearch Function', () => {
    const { SearchBar } = require('../searchBar');
    const event1 = {
      target: {
        name: 'name',
        value: 'value'
      },
      nativeEvent: {
        inputType: ''
      }
    };
    const event2 = {
      target: {
        name: 'name',
        value: 'value\n'
      },
      nativeEvent: {
        inputType: ''
      }
    };
    const event3 = {
      target: {
        name: 'name',
        value: 'value'
      },
      nativeEvent: {
        inputType: 'insertFromPaste'
      }
    };
    const component = shallow(
      <SearchBar
        classes={{}}
        intl={intl}
        onChangeSearch={mockFn}
        onClickSearch={mockFn}
      />
    );
    component.find('#SearchBar').props().onBlur();
    expect(mockFn).toHaveBeenCalledWith('');
    component.find('#SearchBar').props().onChange(event1);
    expect(component.state().name).toEqual(event1.target.value);
    component.find('#SearchBar').props().onChange(event2);
    expect(mockFn).toHaveBeenCalledWith(component.state().searchWord);
    component.find('#SearchBar').props().onChange(event3);
    expect(component.state().name).toEqual(event3.target.value);
  });
  it('on Click search and clear', () => {
    const { SearchBar } = require('../searchBar');
    const component = shallow(
      <SearchBar
        classes={{}}
        intl={intl}
        onChangeSearch={mockFn}
        onClickSearch={mockFn}
      />
    );
    component.find('#SearchBar').prop('InputProps').endAdornment.props.children[0].props.children.props.onClick();
    expect(mockFn).toHaveBeenCalled();
    component.setState({ searchWord: 'searchword' });
    component.find('#SearchBar').prop('InputProps').endAdornment.props.children[1].props.children.props.onClick();
  });
});
