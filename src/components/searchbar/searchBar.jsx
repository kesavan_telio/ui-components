import React, { Component } from 'react'
// import { injectIntl } from 'react-intl'
import { withStyles } from '@material-ui/core/styles'
import { IconButton, TextField, InputAdornment, Icon } from '@material-ui/core'
import PropTypes from 'prop-types'

import Styles from './styles'

export class SearchBar extends Component {
  constructor(props) {
    super()
    this.state = {
      searchWord: props.searchWord,
      prevSearchWord: ''
    }
  }

  componentDidUpdate(prevProps) {
    const { searchWord, name } = this.props
    const { searchWord: searchWordFromState } = this.state
    if (
      searchWord === '' &&
      prevProps.searchWord !== searchWord &&
      searchWordFromState !== '' &&
      name === 'optimizer'
    ) {
      this.onClickClear()
    }
  }

  onChangeSearch = (event) => {
    const { value, name } = event.target
    const { inputType } = event.nativeEvent
    if (inputType === 'insertFromPaste') {
      const keyword = value.replace(/(\n)+/g, ',').slice(0, -1)
      this.setState({ searchWord: keyword })
    }
    if (value.includes('\n')) {
      this.onClickSearch()
    } else {
      this.setState(
        {
          [name]: value
        },
        () => {
          if (!value) this.onClickSearch()
        }
      )
    }
  }

  onClickClear = () => {
    const { onClickSearch } = this.props
    this.setState(
      {
        searchWord: '',
        prevSearchWord: ''
      },
      () => onClickSearch('')
    )
  }

  onClickSearch = () => {
    const { onClickSearch } = this.props
    const { searchWord, prevSearchWord } = this.state
    if (searchWord !== prevSearchWord) {
      this.setState({ prevSearchWord: searchWord })
      onClickSearch(searchWord)
    }
  }

  onBlur = () => {
    const { onChangeSearch } = this.props
    const { searchWord } = this.state
    if (onChangeSearch) onChangeSearch(searchWord)
  }

  render() {
    const { classes, intl, placeholder, customStyle, multiline } = this.props
    const { searchWord } = this.state
    return (
      <TextField
        id='SearchBar'
        placeholder='Demo for POC'
        style={customStyle}
        variant='outlined'
        value={searchWord}
        name='searchWord'
        multiline={multiline}
        rowsMax={1}
        onChange={this.onChangeSearch}
        onBlur={this.onBlur}
        InputProps={{
          classes: {
            root: classes.textFieldRoot,
            input: classes.inputField
          },
          endAdornment: (
            <React.Fragment>
              <InputAdornment>
                <IconButton
                  className={classes.searchIcon}
                  onClick={this.onClickSearch}
                  disabled={!searchWord}
                >
                  <Icon>search</Icon>
                </IconButton>
              </InputAdornment>
              {searchWord && (
                <InputAdornment id='closeButton' position='end'>
                  <IconButton
                    className={classes.iconButton}
                    onClick={this.onClickClear}
                  >
                    <Icon>close</Icon>
                  </IconButton>
                </InputAdornment>
              )}
            </React.Fragment>
          )
        }}
      />
    )
  }
}

SearchBar.propTypes = {
  classes: PropTypes.shape().isRequired,
  onClickSearch: PropTypes.func.isRequired,
  intl: PropTypes.shape().isRequired,
  onChangeSearch: PropTypes.func,
  searchWord: PropTypes.string,
  placeholder: PropTypes.string,
  customStyle: PropTypes.shape(),
  multiline: PropTypes.bool,
  name: PropTypes.string
}

SearchBar.defaultProps = {
  searchWord: '',
  placeholder: 'Search',
  onChangeSearch: null,
  customStyle: {},
  multiline: true,
  name: ''
}

export default withStyles(Styles)(SearchBar)
