import React from 'react';
import { storiesOf } from '@storybook/react';
import { IntlProvider } from 'react-intl';
import { HashRouter as Router } from 'react-router-dom';
import { messagesVi } from '../../translation/locales/vi';
import SearchBarDefault from './searchBar';

const messages = {
  vi: messagesVi
};
const language = localStorage.getItem('language') || 'vi';

storiesOf('SearchBar', module)
  .add('Default', () => (
    <IntlProvider locale={language} messages={messages[language]}>
      <Router>
        <SearchBarDefault />
      </Router>
    </IntlProvider>
  ));
